# Control de Versiones con Git
En este curso se encuentran los recursos relacionados y tareas de la
asignatura.
## Descripción
En este curso se abordan los conceptos básicos y las buenas prácticas
para el control de versiones, así como el uso de la herramienta Git
para facilitar la gestión de un repositorio de código fuente.
## Autor
* <Apellido paterno apellido materno nombre(s)>
### Contacto
<Correo electrónico>